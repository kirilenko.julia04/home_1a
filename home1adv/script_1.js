class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get age() {
        return this._age
    }
    set age(value) {
        this._age = value
    }
    get name() {
        return this._name
    }
    set name(value) {
        this._name = value
    }
}
class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
        this._salary = salary
    }

    get salary() {
        return this._salary *3
    }
    set salary(value) {
        this._salary = value
    }
}
const ivan = new Programmer('Maria', '28', '150', 'fr')
const petro = new Programmer('Polo', '33', '503', 'eng')
const sidar = new Programmer('Kim', '192', '390', 'uk')

console.log(ivan)
console.log(petro)
console.log(sidar)
